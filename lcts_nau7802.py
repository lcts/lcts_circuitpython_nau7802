# SPDX-FileCopyrightText: 2017 Scott Shawcroft, written for Adafruit Industries
# SPDX-FileCopyrightText: Copyright (c) 2020 Christopher Engelhard
#
# SPDX-License-Identifier: MIT
"""
`lcts_nau7802`
================================================================================

Circuitpython I2C driver for the NAU7802 ADC


* Author(s): Christopher Engelhard

Implementation Notes
--------------------

**Hardware:**

.. todo:: Add links to any specific hardware product page(s), or category page(s). Use unordered list & hyperlink rST
   inline format: "* `Link Text <url>`_"

**Software and Dependencies:**

* Adafruit CircuitPython firmware for the supported boards:
  https://github.com/adafruit/circuitpython/releases
* Adafruit's Bus Device library: https://github.com/adafruit/Adafruit_CircuitPython_BusDevice
# * Adafruit's Register library: https://github.com/adafruit/Adafruit_CircuitPython_Register
"""

# imports

__version__ = "0.0.0-auto.0"
__repo__ = "https://gitlab.com/lcts/Lcts_CircuitPython_NAU7802.git"

from micropython import const
import adafruit_bus_device.i2c_device as i2cdevice

class Register:
    """NAU7802 registers

        +-----------------------+------------------------------------------------------------------+
        | Register              | Description                                                      |
        +=======================+==================================================================+
        | ``Register.PU_CTRL``  | Power-up control: Power, clock and reset                         |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.CTRL1``    | Configuration/control Bank 1: LDO, Gain and Conversion Ready     |
		|                       | /DRDY pin functions.                                             |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.CTRL2``    | Configuration/control Bank 2: Channel select, conversion rate    |
		|                       | and calibration                                                  |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.OCAL1_B2`` | Offset calibration value CH1, bits 23:16                         |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.OCAL1_B1`` | Offset calibration value CH1, bits 15:8                          |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.OCAL1_B0`` | Offset calibration value CH1, bits 7:0                           |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.GCAL1_B3`` | Gain calibration value CH1, bits 31:24                           |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.GCAL1_B2`` | Gain calibration value CH1, bits 23:16                           |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.GCAL1_B1`` | Gain calibration value CH1, bits 15:8                            |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.GCAL1_B0`` | Gain calibration value CH1, bits 7:0                             |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.OCAL2_B2`` | Offset calibration value CH2, bits 23:16                         |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.OCAL2_B1`` | Offset calibration value CH2, bits 15:8                          |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.OCAL2_B0`` | Offset calibration value CH2, bits 7:0                           |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.GCAL2_B3`` | Gain calibration value CH2, bits 31:24                           |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.GCAL2_B2`` | Gain calibration value CH2, bits 23:16                           |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.GCAL2_B1`` | Gain calibration value CH2, bits 15:8                            |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.GCAL2_B0`` | Gain calibration value CH2, bits 7:0                             |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.I2C_CTRL`` | I2C configuration                                                |
        +-----------------------+------------------------------------------------------------------+
        | ``Register.ADCO_B2``  | ADC output value, bits 23:16 (read-only)                         |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.ADCO_B1``  | ADC output value, bits 15:8 (read-only)                          |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.ADCO_B0``  | ADC output value, bits 7:0 (read-only)                           |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.ADC_CTRL`` |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.OTP_B1``   | OTP, bits 15:8 (read-only)                                       |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.OTP_B0``   | OTP, bits 7:0 (read-only)                                        |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.PGA_CTRL`` |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.PWR_CTRL`` |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.DIE_REV``  | Device revision code (read-only)                                 |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.MFG_UID``  | Manufacturer UID (read-only)                                     |
        +-----------------------+------------------------------------------------------------------+
		| ``Register.DIE_UID``  | Device UID (read-only)                                           |
        +-----------------------+------------------------------------------------------------------+

    """
    PU_CTRL  = const(0x00)
    CTRL1    = const(0x01)
    CTRL2    = const(0x02)
    OCAL1_B2 = const(0x03)
    OCAL1_B1 = const(0x04)
    OCAL1_B0 = const(0x05)
    GCAL1_B3 = const(0x06)
    GCAL1_B2 = const(0x07) # default 0x80
    GCAL1_B1 = const(0x08)
    GCAL1_B0 = const(0x09)
    OCAL2_B2 = const(0x0A)
    OCAL2_B1 = const(0x0B)
    OCAL2_B0 = const(0x0C)
    GCAL2_B3 = const(0x0D)
    GCAL2_B2 = const(0x0E) # default 0x80
    GCAL2_B1 = const(0x0F)
    GCAL2_B0 = const(0x10)
    I2C_CTRL = const(0x11)
    ADCO_B2  = const(0x12) # read-only
    ADCO_B1  = const(0x13) # read-only
    ADCO_B0  = const(0x14) # read-only
    ADC_CTRL = const(0x15) # also OTP_B2 when read back
	OTP_B1   = const(0x16) # read-only
    OTP_B0   = const(0x17) # read-only
	PGA_CTRL = const(0x1B) # switch between OTP/ADC for 1x15 here
	PWR_CTRL = const(0x1c)
    DIE_REV  = const(0x1F) # read-only
    MFG_UID  = const(0xFE) # read-only
    DIE_UID  = const(0xFF) # read-only

class AnalogVDD:
	INTERNAL = const
